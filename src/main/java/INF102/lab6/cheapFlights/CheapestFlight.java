package INF102.lab6.cheapFlights;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();
        for (Flight flight : flights) {
            City start = flight.start;
            City dest = flight.destination;
            int cost = flight.cost;

            graph.addVertex(start);
            graph.addVertex(dest);
            graph.addEdge(start, dest, cost);
        }
        return graph;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        Map<City, Integer> prices = dijkstra(graph, start, nMaxStops);
        return prices.get(destination);
    }

    private Map<City, Integer> dijkstra(WeightedDirectedGraph<City, Integer> graph, City start, int nMaxStops) {
        Map<ReachInNStops, Trip> bestPrice = new HashMap<>();

        PriorityQueue<Trip> queue = new PriorityQueue<>();

        ReachInNStops startReach = new ReachInNStops(start, 0);
        Trip startTrip = new Trip(startReach, 0);
        addNeighbours(startTrip, graph, queue);
        bestPrice.put(startReach, startTrip);

        while (!queue.isEmpty()) {
            Trip currentTrip = queue.remove();
            if (bestPrice.containsKey(currentTrip.destInNStops)) 
                continue;
            bestPrice.put(currentTrip.destInNStops, currentTrip);
            if (currentTrip.destInNStops.stops <= nMaxStops) 
                addNeighbours(currentTrip, graph, queue);  
        }

        Map<City, Integer> distances = new HashMap<>();
        for (Trip trip : bestPrice.values()) {
            City city = trip.destInNStops.destination;
            int price = trip.totalPrice;
            if (trip.destInNStops.stops > nMaxStops + 1) 
                continue;
            if (!distances.containsKey(city) || distances.get(city) > price) 
                distances.put(city, price);
            
        }
        return distances;
    }


    private void addNeighbours(Trip currentTrip, WeightedDirectedGraph<City, Integer> graph, PriorityQueue<Trip> queue) {
        for (City n : graph.outNeighbours(currentTrip.destInNStops.destination)) {
            ReachInNStops currentReach = new ReachInNStops(n, currentTrip.destInNStops.stops + 1);
            Trip trip = new Trip(currentReach, currentTrip.totalPrice + graph.getWeight(currentTrip.destInNStops.destination, n));
            queue.add(trip);
        }
    }

    class ReachInNStops {
        City destination;
        int stops;

        public ReachInNStops(City dest, int stops) {
            this.destination = dest;
            this.stops = stops;
        }

        @Override
        public boolean equals(Object o){
            if (this == o)
                return true;
            if (!(o instanceof ReachInNStops))
                return false;
            ReachInNStops other = (ReachInNStops) o;
            return this.destination.equals(other.destination) && this.stops == other.stops;

        }

        @Override
        public int hashCode() {
            return Objects.hash(destination, stops);
        }
    }

    class Trip implements Comparable<Trip> {
        ReachInNStops destInNStops;
        Integer totalPrice;
        
        public Trip(ReachInNStops reach, Integer price) {
            this.destInNStops = reach;
            this.totalPrice = price;
        }

        @Override
        public int compareTo(Trip o) {
            return Integer.compare(totalPrice, o.totalPrice);
        } 
    }

}
